package main

import (
	"bufio"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"gitlab.com/lfoliveira.dev/cursodego/EscreverArquivos/model"
)

func main() {

	arquivo, err := os.Open("estados.csv")
	if err != nil {
		fmt.Println("[main] Houve um erro ao abrir o arquivo. Erro: ", err.Error())
		return
	}

	leitorCSV := csv.NewReader(arquivo)
	conteudo, err := leitorCSV.ReadAll()
	if err != nil {
		fmt.Println("[main] Houve um erro ao ler o arquivo CSV. Erro: ", err.Error())
		return
	}

	arquivoJSON, err := os.Create("cidades.json")
	if err != nil {
		fmt.Println("[main] Houve um erro ao criar o arquivo", err.Error())
		return
	}

	escritor := bufio.NewWriter(arquivoJSON)
	escritor.WriteString("[\r\n]")

	for _, linha := range conteudo {
		for indiceItem, item := range linha {
			dados := strings.Split(item, "/")
			cidade := model.Cidade{}
			cidade.Nome = dados[0]
			cidade.Estado = dados[1]
			fmt.Printf("Cidade: %v\r\n", cidade)
			cidadeJSON := json.Marshal(cidade)
			if err != nil {
				fmt.Println("[main] Houve um erro ao gerar o arquvo json", item, err.Error())
			}
			escritor.WriteString("  " + string(cidadeJSON))
			if (indiceItem + 1) < len(linha) {
				escritor.WriteString(",\r\n")
			}
		}
	}

	escritor.WriteString("\r\n]")
	escritor.Flush()
	arquivoJSON.Close()
	arquivo.Close()

}
