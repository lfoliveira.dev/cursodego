package main

import "fmt"

func main() {

	var test [3]int
	test[0] = 3
	test[1] = 2
	test[2] = 1

	fmt.Println(" o array tem", len(test), "posições")

	voices := [2]string{" Luis ", "Rafael"}
	fmt.Printf(" o array tem \n\r%v\r\n", voices)

	citys := [...]string{"Luanda", "Lisboa", "Maputo", "Brasilia"}
	for i, citys := range citys {
		fmt.Printf("[%d] é %s\n\r ", i, citys)
	}

}
