package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/lfoliveira.dev/cursodego/endpoints/SERVER/manipulador"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {

	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb+srv://cursodego:cursodego@cluster0-kilnm.mongodb.net/test?retryWrites=true&w=majority"))
	if err != nil {
		fmt.Println("erro ao subir o banco", err.Error())
		return
	}
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "Meu primeiro server Go")

	})

	http.HandleFunc("/funcao", manipulador.Funcao)
	http.HandleFunc("/ola", manipulador.Ola)
	// http.HandleFunc("/local/", manipulador.Local)

	fmt.Println("------> subiu")

	http.ListenAndServe(":3000", nil)
}
