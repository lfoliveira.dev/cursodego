package main

import (
	"fmt"
	"net/http"

	"gitlab.com/lfoliveira.dev/cursodego/endpoints/BANCO_SQL/repo"
	"gitlab.com/lfoliveira.dev/cursodego/endpoints/SERVER/manipulador"
)

func main() {

	err := repo.AbreConexao()
	if err != nil {
		fmt.Println("erro ao abrir o banco de dados", err.Error())
		return
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "Meu primeiro server Go")

	})

	http.HandleFunc("/funcao", manipulador.Funcao)
	http.HandleFunc("/ola", manipulador.Ola)
	http.HandleFunc("/local/", manipulador.Local)

	fmt.Println("------> subiu")

	http.ListenAndServe(":3000", nil)
}
