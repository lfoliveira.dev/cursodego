package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/lfoliveira.dev/cursodego/endpoints/JSON_UNMARSHAL/model"
)

func main() {

	cliente := &http.Client{
		Timeout: time.Second * 30,
	}
	resposta, err := cliente.Get("https://jsonplaceholder.typicode.com/posts/5")

	request, err := http.NewRequest("GET", "https://jsonplaceholder.typicode.com/posts/5", nil)
	if err != nil {
		fmt.Println("[main] Erro ao criar um request, Erro: ", err.Error())
		return
	}
	request.SetBasicAuth("teste", "teste")
	resposta, err = cliente.Do(request)
	if err != nil {
		fmt.Println("[main] Erro ao abrir pagina do google, Erro: ", err.Error())
		return
	}
	defer resposta.Body.Close()

	if resposta.StatusCode == 200 {
		corpo, err := ioutil.ReadAll(resposta.Body)
		if err != nil {
			fmt.Println("[main] Erro ao ler pagina do google, Erro: ", err.Error())
			return
		}
		fmt.Println("  ")

		post := model.BlogPost{}
		err = json.Unmarshal(corpo, &post)
		if err != nil {
			fmt.Println("[main] JSON no formato invalido, Erro: ", err.Error())
			return
		}
		fmt.Print("O post é: ", post)
	}

}
