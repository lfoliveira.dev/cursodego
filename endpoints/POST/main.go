package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/lfoliveira.dev/cursodego/endpoints/POST/model"
)

func main() {

	cliente := &http.Client{
		Timeout: time.Second * 30,
	}

	usuario := model.Usuario{}
	usuario.ID = 1
	usuario.Nome = "Luis"

	conteudoEnviar, err := json.Marshal(usuario)
	if err != nil {
		fmt.Println("[main] Houve algum erro ao enviar o json, Erro:", err.Error())
		return
	}

	resposta, err := cliente.Get("https://www.google.com.br")
	if err != nil {
		fmt.Println("[main] Erro ao abrir pagina do google, Erro: ", err.Error())
		return
	}
	defer resposta.Body.Close()

	if resposta.StatusCode == 200 {
		corpo, err := ioutil.ReadAll(resposta.Body)
		if err != nil {
			fmt.Println("[main] Erro ao ler pagina do google, Erro: ", err.Error())
			return
		}

		fmt.Println(string(corpo))
	}

	request, err := http.NewRequest("POST", "https://enyzo3eqf5ob.x.pipedream.net", bytes.NewBuffer(conteudoEnviar))
	if err != nil {
		fmt.Println("[main] Erro ao criar um request POST, Erro: ", err.Error())
		return
	}
	request.SetBasicAuth("teste", "teste")
	request.Header.Set("Content-Type", "application/json; charset=utf-8")
	resposta, err = cliente.Do(request)
	if err != nil {
		fmt.Println("[main] Erro ao abrir pagina do google, Erro: ", err.Error())
		return
	}
	defer resposta.Body.Close()

	if resposta.StatusCode == 200 {
		corpo, err := ioutil.ReadAll(resposta.Body)
		if err != nil {
			fmt.Println("[main] Erro ao ler pagina do google, Erro: ", err.Error())
			return
		}
		fmt.Println("  ")
		fmt.Println(string(corpo))
	}

}
