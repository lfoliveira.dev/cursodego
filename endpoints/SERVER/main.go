package main

import (
	"fmt"
	"net/http"

	"gitlab.com/lfoliveira.dev/cursodego/endpoints/SERVER/manipulador"
)

func main() {

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "Meu primeiro server Go")

	})

	http.HandleFunc("/funcao", manipulador.Funcao)
	http.HandleFunc("/ola", manipulador.Ola)

	fmt.Println("------> subiu")

	http.ListenAndServe(":3000", nil)
}
