package manipulador

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/lfoliveira.dev/cursodego/endpoints/SERVER/model"
)

func Ola(w http.ResponseWriter, r *http.Request) {
	hora := time.Now().Format("15:04:05")
	pagina := model.Pagina{}
	pagina.Hora = hora
	if err := Modelos.ExecuteTemplate(w, "ola.html", pagina); err != nil {
		http.Error(w, "Houve um erro na renderização da pagina", http.StatusInternalServerError)
		fmt.Println("[Ola] erro na execução do modelo", err.Error())
	}
}
