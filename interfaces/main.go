package main

import (
	"fmt"

	"gitlab.com/lfoliveira.dev/cursodego/interfaces/model"
)

func main() {

	jojo := model.Ave{}
	jojo.Nome = "JoJo da Silva"

	QueroAcordarComUmCacarejo(jojo)
	QueroOuvirUmaPataNoLago(jojo)

}

func QueroAcordarComUmCacarejo(g model.Galinha) {
	fmt.Println(g.Cacareja())
}

func QueroOuvirUmaPataNoLago(p model.Pato) {
	fmt.Println(p.Quack())
}
