package model

// Galinha representa uma ave do tipo Galinha
type Galinha interface {
	Cacareja() string
}

// Pato representa uma ave do tipo Pato
type Pato interface {
	Quack() string
}

type Ave struct {
	Nome string
}

func (a Ave) Cacareja() string {
	return "Cocórico"
}

func (a Ave) Quack() string {
	return "Quack"
}
