package main

import "fmt"

func main() {

	// var nums []int
	// // fmt.Println(nums, len(nums), cap(nums))

	// nums = make([]int, 5)
	// // fmt.Println(nums, len(nums), cap(nums))

	// capitais := []string{"Lisboa"}
	// // fmt.Println(capitais, len(capitais), cap(capitais))

	// capitais = append(capitais, "brasilia", "MAnaus")
	// // fmt.Println(capitais, len(capitais), cap(capitais))

	cidade := make([]string, 5)
	cidade[0] = "Nova York"
	cidade[1] = "Singapura"
	cidade[2] = "Madeira"
	cidade[3] = "Guaraci"
	cidade[4] = "São Paulo"
	fmt.Println(cidade, len(cidade), cap(cidade))

	for i, cidade := range cidade {
		fmt.Printf("Cidade[%d] = %s\n\r", i, cidade)
	}

	capitaisAsia := cidade[3:5]
	fmt.Println(capitaisAsia, len(capitaisAsia), cap(capitaisAsia))

	temp1 := cidade[:2]
	fmt.Println(temp1, len(temp1), cap(temp1))

	temp2 := cidade[len(cidade)-2:]
	fmt.Println(temp2, len(temp2), cap(temp2))

	indiceARetirar := 2
	temp := cidade[:indiceARetirar]
	temp = append(temp, cidade[indiceARetirar+1:]...)
	copy(cidade, temp)
	fmt.Println(cidade)

}
